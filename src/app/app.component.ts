import { Component } from '@angular/core';

@Component({
  selector: 'pm-app',
  templateUrl: 'app.component.html',
  styleUrls: [
    'app.component.scss'
  ],
  providers: []
})
export class AppComponent {
  pageTitle: string = 'Acme Product Management';
}

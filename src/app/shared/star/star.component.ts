import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'ai-star',
  moduleId: module.id,
  templateUrl: 'star.component.html',
  styleUrls: ['star.component.scss']
})
export class StarComponent implements OnInit, OnChanges {

  @Input() rating: number = 4.5;
  @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();

  starWidth: number;

  constructor() { }

  ngOnInit() {
    this.calculateWidth();
  }

  ngOnChanges(): void {
    this.calculateWidth();
  }

  calculateWidth(): void {
    this.starWidth = this.rating * 86/5;
  }

  onClick(): void {
    this.ratingClicked.emit(`The rating ${this.rating} was clicked.`);
  }

}

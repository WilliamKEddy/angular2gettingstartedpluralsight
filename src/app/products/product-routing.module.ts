import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductListComponent } from './product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductDetailGuard } from './product.guard';

const routes: Routes = [
  { path: 'products', component: ProductListComponent },
  {
    path: 'product/:id',
    component: ProductDetailComponent,
    canActivate: [ProductDetailGuard]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ],
  providers: []
})
export class ProductRoutingModule { }
